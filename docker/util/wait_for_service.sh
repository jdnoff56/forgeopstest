#!/bin/sh
# Wait for service and port

while true
do
    echo "Waiting for $1:$2 (errors expected if the config store is not yet up...)"
    nc -zv $1 $2
    if [ $? -ne 0 ];
    then
        sleep 10
    else
        echo "Service $1:$2 is up"
        exit 0
    fi
done